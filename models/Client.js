var keystone = require('keystone');

/**
 * Client Model
 * ==================
 */

var Client = new keystone.List('Client', {
	autokey: { path: 'slug', from: 'name', unique: true }
});

Client.add({
	name: { type: String, required: true },
	url: { type: String, required: false }
});

Client.relationship({
	ref: 'Project',
	path: 'projects',
	refPath: 'client'
});

Client.defaultColumns = 'name, url|20%';
Client.register();
