var keystone = require('keystone');

/**
 * NoteCategory Model
 * ==================
 */

var NoteCategory = new keystone.List('NoteCategory', {
	autokey: { from: 'name', path: 'key', unique: true },
});

NoteCategory.add({
	name: { type: String, required: true },
});

NoteCategory.relationship({ ref: 'Note', path: 'notes', refPath: 'categories' });

NoteCategory.register();
