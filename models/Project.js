var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Project Model
 * ==========
 */

var Project = new keystone.List('Project', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Project.add({
	title: { type: String, required: true },
	state: {
		type: Types.Select,
		options: 'draft, published, archived',
		default: 'draft',
		index: true
	},
	author: { type: Types.Relationship, ref: 'User', index: true },
	client: { type: Types.Relationship, ref: 'Client', index: true },
	url: { type: String },
	publishedDate: {
		type: Types.Date,
		index: true,
		dependsOn: { state: 'published' }
	},
	image: { type: Types.CloudinaryImage },
	content: {
		brief: { type: String, initial: true, required: true },
		extended: { type: Types.Html, wysiwyg: true, height: 400, default: 'Situation/Task/Action/Result' }
	},
	categories: {
		type: Types.Relationship,
		ref: 'ProjectCategory',
		many: true
	},
	tools: { type: Types.Relationship, ref: 'Tool', many: true },
	year: { type: Types.Date, index: true, required: true, default: new Date() }
});

Project.schema.virtual('content.full').get(function() {
	return this.content.extended || this.content.brief;
});

Project.defaultColumns =
	'title, client|20%, state|20%, author|20%, publishedDate|20%';
Project.register();
