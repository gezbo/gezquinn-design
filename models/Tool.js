var keystone = require('keystone');

/**
 * Tool Model
 * ==================
 */

var Tool = new keystone.List('Tool', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Tool.add({
	name: { type: String, required: true }
});

Tool.relationship(
	{
		ref: 'Project',
		path: 'projects',
		refPath: 'tools'
	},
	{
		ref: 'Note',
		path: 'notes',
		refPath: 'tools'
	}
);

Tool.register();
