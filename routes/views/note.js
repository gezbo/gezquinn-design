var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'Notebook';
	locals.filters = {
		note: req.params.note,
	};
	locals.data = {
		notes: [],
	};

	// Load the current note
	view.on('init', function (next) {

		var q = keystone.list('Note').model.findOne({
			state: 'published',
			slug: locals.filters.note,
		}).populate('author categories');

		q.exec(function (err, result) {
			locals.data.note = result;
			next(err);
		});

	});

	// Load other notes
	view.on('init', function (next) {

		var q = keystone.list('Note').model.find().where('state', 'published').sort('-publishedDate').populate('author').limit('4');

		q.exec(function (err, results) {
			locals.data.notes = results;
			next(err);
		});

	});

	// Render the view
	view.render('note');
};
